//
//  IntroView.swift
//  WashHandOfficial
//
//  Created by Vincenzo Guida on 24/02/2020.
//  Copyright © 2020 Vincenzo Guida. All rights reserved.
//

import SwiftUI

struct IntroView: View {
    var body: some View {
        VStack{
            Text("WashReminder ti dà il benvenuto")
            VStack{
                VStack{
                    Text("Un app che si prende cura di te,  ogni ora")
                    Text("Non ti preoccupare potrai modificare la cadenza delle notifiche nelle impostazioni.")
                }
                VStack{
                    Text("L’igiene con te, sempre.")
                    Text("Non ti preoccupare potrai modificare la cadenza delle notifiche nelle impostazioni.")
                }
                VStack{
                    Text("Modalità COVID-19 (Corona Virus)")
                    Text("Non cadere nel terrore, aiuterai il virus a diffondersi")
                }
            }
        }
    }
}

struct IntroView_Previews: PreviewProvider {
    static var previews: some View {
        IntroView()
    }
}
