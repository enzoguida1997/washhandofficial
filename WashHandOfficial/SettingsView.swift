//
//  SettingsView.swift
//  WashHandOfficial
//
//  Created by Alessio Petrone on 25/02/2020.
//  Copyright © 2020 Vincenzo Guida. All rights reserved.
//

import SwiftUI

struct SettingsView: View {

    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        NavigationView {
            Text("Settings")
            
            .navigationBarTitle("Reminder")
            .navigationBarItems(trailing:
                Button("Close") {
                    self.presentationMode.wrappedValue.dismiss()
                }
            )
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
