//
//  UserData.swift
//  WashHandOfficial
//
//  Created by Vincenzo Guida on 23/02/2020.
//  Copyright © 2020 Vincenzo Guida. All rights reserved.
//
//import SwiftUI
//import Combine
//
//struct Defaults{
//static let (countKey, dateKey) = ("count", "address")
//static let userSessionKey = "com.save.usersession"
//private static let userDefault = UserDefaults.standard
//
///**
//   - Description - It's using for the passing and fetching
//                user values from the UserDefaults.
// */
//struct UserDetails {
//    let count: String
//    let nowDate: String
//
//    init(_ json: [String: String]) {
//        self.count = json[countKey] ?? ""
//        self.nowDate = json[dateKey] ?? ""
//    }
//}
//
///**
// - Description - Saving user details
// - Inputs - name `String` & address `String`
// */
//static func save(_ count: String, nowDate: String){
//    userDefault.set([countKey: count, countKey: nowDate],
//                    forKey: userSessionKey)
//}
//
///**
// - Description - Fetching Values via Model `UserDetails` you can use it based on your uses.
// - Output - `UserDetails` model
// */
//static func getNameAndAddress()-> UserDetails {
//    return UserDetails((userDefault.value(forKey: userSessionKey) as? [String: String]) ?? [:])
//}
//
///**
//    - Description - Clearing user details for the user key `com.save.usersession`
// */
//static func clearUserData(){
//    userDefault.removeObject(forKey: userSessionKey)
//}
//}


struct UserDefault: Codable{
    var count: Int = 0
    var show_modal: Bool = true
    var dataToday: String = ""
    var notificationStart: String = ""
    var notificationEnd: String = ""
    var notificationTime: Int = 10800
    var notficationEnabled: Bool = true
}
