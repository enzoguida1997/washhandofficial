//
//  FloatingBanner.swift
//  WashHandOfficial
//
//  Created by Vincenzo Guida on 04/03/2020.
//  Copyright © 2020 Vincenzo Guida. All rights reserved.
//

import SwiftUI

struct FloatingBadgeComponent: View {
    struct Constants {
        static let padding: CGFloat = 10
        static let cornerRadius: CGFloat = 10
        static let actionIcon: String = "xmark"
    }

    let text: String
    let onActionTapGesture: () -> Void

    init(text: String, onActionTapGesture: @escaping () -> Void) {
        self.text = text
        self.onActionTapGesture = onActionTapGesture
    }

    var body: some View {
        
        return HStack {
            HStack {
                Text(text)
                    .foregroundColor(.white)
                    .font(.caption)
                    .multilineTextAlignment(.center)
           
            }
            .padding(.vertical, Constants.padding)
            .padding(.horizontal, Constants.padding)
            .background(Color.blue)
            .cornerRadius(Constants.cornerRadius)
            .shadow(color: Color.blue, radius: 5, x: 0, y: 0)
        }
        .padding(.top, 10)
        .transition(.asymmetric(insertion: .move(edge: .top), removal: .move(edge: .top)))
        .animation(.easeInOut(duration: 0.50))
    }
}

struct FloatingBadgeComponent_Previews: PreviewProvider {
    static var previews: some View {
        FloatingBadgeComponent(text: "Bravo! 🎉", onActionTapGesture: {})
    }
}
