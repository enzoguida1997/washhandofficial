//
//  ContentView.swift
//  WashHandOfficial
//
//  Created by Vincenzo Guida on 23/02/2020.
//  Copyright © 2020 Vincenzo Guida. All rights reserved.
//

import SwiftUI
import UserNotifications


struct ContentView: View {
    @State var washCounter = 0
    @State private var user = UserDefault()
    @State private var countWash:Int = 0
    
    @State private var showSettings: Bool = false
    
    @State private var notificationBool:Bool = false
    @State private var timeInterval: Double = 5.0
    @State private var show_modal: Bool = false
    @State private var show: Bool = false
    

    var body: some View {
        ZStack(alignment: .top){
            if show {
                                 FloatingBadgeComponent(text: "Bravo! 🎉", onActionTapGesture: {})

                      }
          
            VStack {
                
                VStack(alignment: .leading){
            Text("Washed Hand")
                    .font(.system(size: 34, weight: .bold, design: .rounded))
            Text("Total of today")
                    .font(.system(size: 20, weight: .regular, design: .rounded))
            Text(String(self.user.count))
                .font(.system(size: 120, weight: .bold, design: .rounded))
                }
            Button(action: {
                self.show.toggle()

                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.show.toggle()
                }
                self.user.count += 1
                UserDefaults.standard.set(self.user.count, forKey: "count")
//                self.countWash += 1
                let today = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "dMMy"
//                let date = formatter.date(from: user.dataToday)!
                self.user.dataToday = formatter.string(from: today)
                
                UserDefaults.standard.set(self.user.dataToday, forKey: "dataToday")
                self.scheduleLocal()
                   }) {
                       HStack(spacing: 20) {
                           Image(systemName: "plus.circle.fill")
                           .resizable()
                           .frame(width: 20, height: 20)
                           Text("Aggiungi")
                       }
                   }
                   .frame(width: 120, height: 50)
            Button(action: {
            //                self.scheduleNotification()
                            self.user.count -= 1
            //                self.countWash += 1
                            let today = Date()
                            let formatter = DateFormatter()
                            formatter.dateFormat = "dMMy"
            //                print(formatter.string(from: today))
                            self.user.dataToday = formatter.string(from: today)
                            print(formatter.string(from: today))
                            UserDefaults.standard.set(self.user.count, forKey: "count")
                               }) {
                                   HStack(spacing: 20) {
                                       Image(systemName: "plus.circle.fill")
                                       .resizable()
                                       .frame(width: 20, height: 20)
                                       Text("Annulla")
                                   }
                               }
                               .frame(width: 120, height: 50)


        }.onAppear(){
            NotificationCenter.default.addObserver(forName: NSNotification.Name("AddWash"), object: nil, queue: .main) { (_) in

                self.user.count += 1
                UserDefaults.standard.set(self.user.count, forKey: "count")
            }
            guard let getCount = UserDefaults.standard.value(forKey: "count")
                else{
                    return
            }
           
            
            self.user.count = getCount as! Int
            print(self.user.count)
            guard let getData = UserDefaults.standard.value(forKey: "dataToday")
                       else{
                           return
                   }
            
                self.user.dataToday = getData as! String
            print(self.user.dataToday)
            self.calcDayCounterReset()
            }
            
        }.sheet(isPresented: self.$show_modal) {
         IntroView()
    
        }
    }




//    func scheduleNotification(){
//
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge]) { (_, _) in
//
//        }
//        let content = UNMutableNotificationContent()
//        content.title = "Lavati le mani"
//
//        content.body = "Non si può mai sapere 🤷🏻‍♂️"
//
//        let done = UNNotificationAction(identifier: "done", title: "Mi sono lavato le mani! 😅", options: .foreground)
//
//        let notNow = UNNotificationAction(identifier: "notNow", title: "Riproponi tra 5 minuti", options: .authenticationRequired)
//
//        let cancel = UNNotificationAction(identifier: "cancel", title: "Dismiss", options: .destructive)
//
//        let categories = UNNotificationCategory(identifier: "action", actions: [done,notNow,cancel], intentIdentifiers: [])
//        UNUserNotificationCenter.current().setNotificationCategories([categories])
//        content.categoryIdentifier = "action"
//        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: notificationBool)
//
//        let req = UNNotificationRequest(identifier: "req", content: content, trigger: trigger)
//        UNUserNotificationCenter.current().add(req, withCompletionHandler: nil)
//    }
    
    func scheduleLocal() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge]) { (_, _) in

              }
     

        let content = UNMutableNotificationContent()
        content.title = "Lavati le mani"

        content.body = "Non si può mai sapere 🤷🏻‍♂️"

        let done = UNNotificationAction(identifier: "done", title: "Mi sono lavato le mani! 😅", options: .foreground)

//        let notNow = UNNotificationAction(identifier: "notNow", title: "Riproponi tra 5 minuti", options: .authenticationRequired)
        let remindMeLaterAction = UNNotificationAction(identifier: "REMIND_ME_LATER_ACTION",
        title: "Riproponi tra 5 minuti",
        options: UNNotificationActionOptions(rawValue: 0))

        let cancel = UNNotificationAction(identifier: "cancel", title: "Dismiss", options: .destructive)

        let categories = UNNotificationCategory(identifier: "action", actions: [done,remindMeLaterAction,cancel], intentIdentifiers: [], options: .customDismissAction)
        UNUserNotificationCenter.current().setNotificationCategories([categories])
        content.categoryIdentifier = "action"
        var dateComponents = DateComponents()
        dateComponents.hour = 14
        dateComponents.minute = 00
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
//        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        let trigger14 = "trigger14"
        let request = UNNotificationRequest(identifier: trigger14, content: content, trigger: trigger)
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.add(request) { (error) in
           if error != nil {
             print("ErroreNotifiche")
           }
        }
    }

    // Funzione che calcola se si è passati al giorno successivo e se positivo resetta il counter
    // delle mani lavate.
    func calcDayCounterReset(){
       
        let today = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dMMy"
//        let calendar = Calendar.current
//        let components = calendar.dateComponents([.day, .month, .year], from: today)
//        let finalDate = calendar.date(from:components)!
        if formatter.string(from: today) > user.dataToday{
            print("vero")
            self.user.count = 0
            UserDefaults.standard.set(self.user.count, forKey: "count")
            
        } else {
            print("falso")
            return
        }
//        return ConcentricOnboardingView(pages: <your_pages>, bgColors: <your_colors>)
    }
    
    

    
//    func scheduleNotification2() {
//
//           var dateComponents = DateComponents()
//           dateComponents.calendar = Calendar.current
//
//        dateComponents.hour = 12
//        dateComponents.minute = 06// 14:00 hours
//
//           // Create the trigger as a repeating event.
//           let trigger = UNCalendarNotificationTrigger(
//           dateMatching: dateComponents, repeats: true)
//
//
//           let content = UNMutableNotificationContent()
//           content.title = "Lavati le mani"
//
//           content.body = "Non si può mai sapere 🤷🏻‍♂️"
//
//        content.sound = UNNotificationSound.default
//           content.categoryIdentifier = "todoList"
//
//           let request = UNNotificationRequest(identifier: "textNotification", content: content, trigger: trigger)
//
//           //UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
//           UNUserNotificationCenter.current().add(request) {(error) in
//               if let error = error {
//                   print("Uh oh! We had an error: \(error)")
//               }
//           }
//       }
 
        
    
}


//func scheduleNotification() {
//    let content = UNMutableNotificationContent()
//    content.title = "Lavati la mano!"
//
//    let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.day, .hour, .minute], from: ), repeats: false)
//
//    let identifier = "Local Notification"
//    let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
//
//    notificationCenter.add(request) { (error) in
//        if let error = error {
//            print("Error \(error.localizedDescription)")
//        }
//    }
//}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


